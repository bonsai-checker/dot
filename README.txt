Prerequisites:
- Racket 6.5 or higher
  [ http://racket-lang.org ]
- Rosette 2.1 or higher
  [ http://emina.github.io/rosette/ ]



Running the examples
- NanoDOT
  $ time racket nanodot.rkt
  The file is currently configured with the "bug" that when creating an
  intersection, the typechecker neglects to check that the two records being
  combined have disjoint domains (this check is present in the DOT spec).

- NanoScala
  $ time racket nanoscala.rkt
  The file is currently configured to find a counterexample for SI-9633, by
  adding a NULL value.



Source index
- bounded.rkt   : Monadic bounding macros
- nanodot.rkt   : NanoDOT
- nanoscala.rkt : NanoScala
- tree-lib.rkt  : Bonsai's symbolic tree library
